import os, sys
import svgwrite
import random as r

from TreeRocket import *


def savePathToSvgFile(fileName, rockets):
    '''
    Function used to save a single path as a SVG file
    '''
    svg = svgwrite.Drawing(filename=fileName, size = (500, 500), debug=True)
    for r in rockets:
        originalPoint = "M" + str(r.coordinates[0][0]) + "," + str(r.coordinates[0][1])
        path = svgwrite.path.Path(originalPoint, fill='none', stroke='black', stroke_width=r.size)
        for i in range(1, len(r.coordinates)):
            path.push("L%d,%d" % r.coordinates[i])
        svg.add(path)
    svg.save()


def generateTree(time):
    '''
    Main Function for drawing the tree, will take a number of future parameters
    including those for the initial rocket.
    '''
    coordinates = []
    #initialisations should be replaced with random values
    t = TreeRocket(190,500,4,-31, 10.0)
    rockets = [t, TreeRocket(240,500,-7,-26,12.0)]
    split = False
    for i in range(time):
        new_rockets = []
        for r in rockets:
            #following logic should be replaced with random values
            if i > 10 and i % 6 == 0:
                new_rockets += (r.explode(5))
                split = True
            r.update(0.981, 1.0)
        rockets += new_rockets

    savePathToSvgFile('tree.svg', rockets)



if __name__ == '__main__':
    if len(sys.argv) == 1:
        r.seed(0)
    else:
        r.seed(sys.argv[1])
    generateTree(35)
