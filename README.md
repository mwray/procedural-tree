# Procedural Trees Using Rockets #
For the competition on [reddit](https://www.reddit.com/r/proceduralgeneration/comments/49wye4/monthly_challenge_4_march_2016_procedural/).

The idea is that modelling a firework (made up of many fireworks) and following the trails certain parameter combinations will make them look like trees, maybe not but who knows!.

### Get Started ###
* Requires python 2.7.
* Download latest commit on master and run:
* pip install svgwrite
* python ProceduralTree.py
* This will save a .svg file  called Tree.svg which will generate a tree-ish thing.