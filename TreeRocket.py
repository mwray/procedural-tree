class TreeRocket:
    def __init__(self, x, y, velX, velY, size):
        '''
        Constructor for the TreeRocket class
        @param x: float value which represents the rocket's starting x position
        @param y: float value which represents the rocket's starting y position
        @param velX: float value of the rocket's starting x velocity
        @param velY: float value of the rocket's y velocity
        '''
        self.velX = velX
        self.velY = velY
        self.x = x
        self.y = y
        self.size = size
        self.coordinates = [(x,y)]
        self.active = True


    def update(self, gravity, deltaT):
        '''
        Update function which is called every tick to track the rocket's
        position.
        @param gravity: the strength of gravity effecting the rocket.
        @param deltaT: The length of time that has passed since the last tick.
        '''
        if self.active:
            self.velY += gravity * deltaT
            self.x += self.velX * deltaT
            self.y += (self.velY) * deltaT
            self.coordinates.append((self.x, self.y))

    def explode(self, num, active=False):
        '''
        Will create num new rockets and return them. Rockets are created based
        upon this rocket's current position and velocity.
        @param num: The number of new rockets to create.
        @param active (default False): Whether this rocket should remain active
            after it explodes.
        @returns: list of rockets, of size num, which were created.
        '''
        self.active = active
        new_rockets = []
        for i in range(num):
            new_rockets.append(TreeRocket(self.x, self.y, self.velX + i, self.velY + i, self.size / 2))
        return new_rockets
